import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Picker,
  ScrollView,
  Image,
} from 'react-native';
import { Headline } from 'react-native-paper';
import { TextInput } from 'react-native-paper';
import { Button } from 'react-native-paper';
import { Switch } from 'react-native-paper';
import { Card } from 'react-native-paper';
import { Subheading } from 'react-native-paper';
import { RadioButton } from 'react-native-paper';
import { Appbar } from 'react-native-paper';

export default class App extends React.Component {
  state = {
    name: '',
    regno: '',
    email: '',
    registerid: '',
    password: '',
    phoneno: '',
    collegename: '',
    isSwitchOn: false,
    city: '',
    states: 'Tamilnadu',
    checked: 'CSE',
  };

  login = (
    text1,
    text2,
    text3,
    text4,
    text5,
    text6,
    text7,
    text8,
    text9,
    text10,
    text11
  ) => {
    alert(
      text1 +
        '\n' +
        text2 +
        '\n' +
        text3 +
        '\n' +
        text4 +
        '\n' +
        text5 +
        '\n' +
        text6 +
        '\n' +
        text7 +
        '\n' +
        text8 +
        '\n' +
        text9 +
        '\n' +
        text10 +
        '\n' +
        text11
    );
  };
  render() {
    const { isSwitchOn } = this.state;
    const { checked } = this.state;
    return (
      <ScrollView contentContainerStyle={{ height: 1250 }}>
        <View>
          <Card>
            <Appbar.Header style={styles.background}>
              <Appbar.Content
                title="Internship Registration Form"
                style={styles.letter}
              />
            </Appbar.Header>
          </Card>
          <Card>
          <View style={styles.view}>
            <Image
              style={styles.image}
              source={require('./components/11.png')}
            />
            </View>
            <Card.Content>
              <TextInput
                mode="outlined"
                label="Name"
                value={this.state.name}
                onChangeText={name => this.setState({ name })}
              />
              <Text />
              <TextInput
                label="Phone Number"
                mode="outlined"
                value={this.state.phoneno}
                onChangeText={phoneno => this.setState({ phoneno })}
              />
              <Text />
              <TextInput
                label="Email"
                mode="outlined"
                value={this.state.email}
                onChangeText={email => this.setState({ email })}
              />

              <Text />
              <TextInput
                label="RegisterID for Intern"
                mode="outlined"
                value={this.state.registerid}
                onChangeText={registerid => this.setState({ registerid })}
              />
              <Text />
              <TextInput
                label="Password"
                mode="outlined"
                value={this.state.password}
                onChangeText={password => this.setState({ password })}
              />

              <Text />
              <TextInput
                label="Confirm Password "
                mode="outlined"
                value={this.state.confirmpassword}
                onChangeText={confirmpassword =>
                  this.setState({ confirmpassword })
                }
              />
              <Text />
              <Subheading>College Details</Subheading>
              <TextInput
                label="College Name"
                mode="outlined"
                value={this.state.collegename}
                onChangeText={collegename => this.setState({ collegename })}
              />
              <TextInput
                label="College Register-No"
                mode="outlined"
                value={this.state.regno}
                onChangeText={regno => this.setState({ regno })}
              />
              <Text />
              <Subheading>Department:</Subheading>
              <View style={styles.flex}>
                <RadioButton
                  value="cse"
                  mode="outlined"
                  status={checked === 'cse' ? 'checked' : 'unchecked'}
                  onPress={() => {
                    this.setState({ checked: 'cse' });
                  }}
                />
                <Subheading>CSE</Subheading>
                <RadioButton
                  value="it"
                  mode="outlined"
                  status={checked === 'it' ? 'checked' : 'unchecked'}
                  onPress={() => {
                    this.setState({ checked: 'it' });
                  }}
                />
                <Subheading>IT</Subheading>
                <RadioButton
                  value="ece"
                  mode="outlined"
                  status={checked === 'ece' ? 'checked' : 'unchecked'}
                  onPress={() => {
                    this.setState({ checked: 'ece' });
                  }}
                />
                <Subheading>ECE</Subheading>
                <RadioButton
                  value="eee"
                  mode="outlined"
                  status={checked === 'eee' ? 'checked' : 'unchecked'}
                  onPress={() => {
                    this.setState({ checked: 'eee' });
                  }}
                />
                <Subheading>EEE</Subheading>
              </View>
              <Text />
              <TextInput
                label="City"
                mode="outlined"
                value={this.state.city}
                onChangeText={city => this.setState({ city })}
              />
              <Text />
              <Subheading> STATE</Subheading>
              <Picker
                selectedValue={this.state.states}
                style={{ height: 50, width: 300 }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ states: itemValue })
                }>
                <Picker.Item label="TamilNadu" value="Tamilnadu" />
                <Picker.Item label="Kerala" value="Kerala" />
                <Picker.Item label="Andhra Pradhesh" value="Andhra" />
                <Picker.Item label="Karnataka" value="Karnataka" />
              </Picker>
              <Text />
              <View style={styles.flex}>
                <Text>I agree the Terms & Conditons </Text>
                <Switch
                  value={isSwitchOn}
                  onValueChange={() => {
                    this.setState({ isSwitchOn: !isSwitchOn });
                  }}
                />
              </View>
              <Text />
              <Button
                style={styles.background}
                mode="contained"
                onPress={() =>
                  fetch('http://192.168.72.44:5000/users/', {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      Name: this.state.name,
                      Register: this.state.regno,
                      Email: this.state.email,
                      RegisterId: this.state.registerid,
                      Password: this.state.password,
                      Phone: this.state.phoneno,
                      CollegeName: this.state.collegename,
                      Swith: this.state.isSwitchOn,
                      City: this.state.city,
                      States: this.state.states,
                      Checked: this.state.checked,
                    }),
                  })
                }>
                Submit
              </Button>
            </Card.Content>
          </Card>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  flex: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  view: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop:5,
  },
  image: {
    height: 100,
    width: 350,
    
  }, 
  background: {
    left: 0,
    right: 0,
    backgroundColor: '#3291BB',
    color: 'white',
    align: 'center',
  },
  letter: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
